---
title: First post!
subtitle: Let's try how this does work
date: 2018-11-11
tags: ["offtopic", "fun"]
---

This is my first post, and today is **2018/11/11**. What a coincidence!

Seems that in numerology, this _combination_ of elevens could mean something. Would it be related to some kind of binary messages? Will we be able to understand them?

<!--more-->

People loves coincidences and here we do have a funny one, apparently, as you can read in [wikipedia](https://en.wikipedia.org/wiki/11:11_(numerology)):

>For various reasons, people ascribe different kinds of significance to dates and
>numbers; for example, the date of November 11, 2011, or "11/11/11", showed an
>increase in the number of marriages taking place in different areas throughout
>the world, including the U.S. and across the Asian continent. Many females
>were pregnant at this time, and babies born on this date also received special
>media attention.

From now on, you have to try starting your statements with `'for various reasons'` during a talk/meeting with your peeps, this way you will have the moral authorization to say whatever you want!

------
